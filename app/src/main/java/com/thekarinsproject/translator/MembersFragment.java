package com.thekarinsproject.translator;


import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class MembersFragment extends Fragment {

    MediaPlayer mMediaPlayer;

    // initialization of audio attributes and focus request
    AudioManager mAudioManager;

    final Object focusLock = new Object(); // No idea what is this

    boolean playbackDelayed = false;
    boolean playbackNowAuthorized = false;
    boolean resumeOnFocusGain = false;

    /**
     * Creates a new listener that manages the focus of the MediaPlayer, pausing, resuming
     * or stopping the player depending on it, this happens after the access
     * to the Audio system is granted */
    AudioManager.OnAudioFocusChangeListener mOnAudioFocusChangeListener =
            new AudioManager.OnAudioFocusChangeListener() {
                @Override
                public void onAudioFocusChange(int focusChange) {

                    switch(focusChange) {
                        case AudioManager.AUDIOFOCUS_GAIN:
                            if(playbackDelayed || resumeOnFocusGain){
                                synchronized (focusLock) {
                                    playbackDelayed = false;
                                    resumeOnFocusGain = false;
                                }
                                mMediaPlayer.start();
                            }
                            break;
                        case AudioManager.AUDIOFOCUS_LOSS:
                            synchronized (focusLock) {
                                resumeOnFocusGain = false;
                                playbackDelayed = false;
                            }
                            releaseMediaPlayer();
                            break;
                        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                            synchronized (focusLock) {
                                resumeOnFocusGain = true;
                                playbackDelayed = false;
                            }
                            mMediaPlayer.pause();
                            mMediaPlayer.seekTo(0);
                            break;
                    }
                }
            };

    AudioAttributes mAudioAttributes;
    AudioFocusRequest mAudioFocusRequest;

    public MembersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_members, container, false);

        final ArrayList<Word> members = new ArrayList<>();

        members.add(new Word(R.drawable.family_father,"Father", "Père", R.raw.pere));
        members.add(new Word(R.drawable.family_mother,"Mother", "Mère", R.raw.mere));
        members.add(new Word(R.drawable.family_younger_brother,"Brother", "Frère", R.raw.frere));
        members.add(new Word(R.drawable.family_younger_sister,"Sister", "Soeur", R.raw.soeur));
        members.add(new Word(R.drawable.family_son,"Son", "Fils", R.raw.fils));
        members.add(new Word(R.drawable.family_daughter,"Daughter", "Fille", R.raw.fille));
        members.add(new Word(R.drawable.family_grandmother,"Grandmother", "Grand-mère", R.raw.grandmere));
        members.add(new Word(R.drawable.family_grandfather,"Grandfather", "Grand-père", R.raw.grandpere));
        members.add(new Word(R.drawable.family_older_brother,"Uncle", "Oncle", R.raw.oncle));
        members.add(new Word(R.drawable.family_older_sister,"Aunt", "Tante", R.raw.tante));


        WordAdapter itemsAdapter = new WordAdapter(getActivity(), members, R.color.category_members);

        ListView listView = rootView.findViewById(R.id.list);

        listView.setAdapter(itemsAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // releases the media player if it currently exists because we will play another sound file
                releaseMediaPlayer();

                // Initialising audio elements;
                mAudioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
                mAudioAttributes = new AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_MEDIA)
                        .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                        .build();
                mAudioFocusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN_TRANSIENT)
                        .setAudioAttributes(mAudioAttributes)
                        .setAcceptsDelayedFocusGain(true)
                        .setOnAudioFocusChangeListener(mOnAudioFocusChangeListener)
                        .build();

                /* Verifies if audio focus has been granted or not */
                int response = mAudioManager.requestAudioFocus(mAudioFocusRequest);
                synchronized(focusLock) {
                    if(response == AudioManager.AUDIOFOCUS_REQUEST_FAILED) {
                        playbackNowAuthorized = false;
                    } else if (response == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                        playbackNowAuthorized = true;
                        mMediaPlayer = MediaPlayer.create(getActivity(), members.get(position).getAudioId());
                        mMediaPlayer.start();
                    } else if (response == AudioManager.AUDIOFOCUS_REQUEST_DELAYED) {
                        playbackDelayed = true;
                        playbackNowAuthorized = false;
                    }
                }
                //Toast.makeText(NumberActivity.this, "List clicked", Toast.LENGTH_SHORT).show(); // Displays a message
                mMediaPlayer.setOnCompletionListener(mCompletionListener);
            }
        });

        return rootView;
    }

    /**
     * This listener gets triggered when the {@link MediaPlayer}
     * has completed playing the audio file */

    private MediaPlayer.OnCompletionListener mCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            releaseMediaPlayer();
        }
    };

    @Override
    public void onStop() {
        super.onStop();
        releaseMediaPlayer();
    }

    public void releaseMediaPlayer() {
        if(mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
            // Releases the focus
            mAudioManager.abandonAudioFocusRequest(mAudioFocusRequest);
        }
    }

}
