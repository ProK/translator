package com.thekarinsproject.translator;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhrasesFragment extends Fragment {


    public PhrasesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_phrases, container, false);

        ArrayList<Word> phrases = new ArrayList<>();

        phrases.add(new Word("Hello!", "Bonjour!"));
        phrases.add(new Word("Where are you going?", "Oú allez vous?"));
        phrases.add(new Word("What\'s your name", "Comment vous appellez vous"));
        phrases.add(new Word("My name is...", "Je m'appel..."));
        phrases.add(new Word("How are you?", "Comment allez vous?"));
        phrases.add(new Word("I\'m fine, thank you", "Ça va, merci"));
        phrases.add(new Word("How old are you?", "Quel âge avez vous?"));
        phrases.add(new Word("Have a nice day!", "Passe une bonne journée"));
        phrases.add(new Word("I'm sorry", "Je suis désolé"));
        phrases.add(new Word("Excuse me", "Excusez moi"));

        WordAdapter itemsAdapter = new WordAdapter(getActivity(), phrases, R.color.category_phrases);

        ListView listView = rootView.findViewById(R.id.list);

        listView.setAdapter(itemsAdapter);
        return rootView;
    }

}
