package com.thekarinsproject.translator;

import android.graphics.drawable.Icon;
import android.media.Image;

public class Word {

    private String englishWord, frenchWord;
    private final int NO_IMAGE = -1;
    private int imageId = NO_IMAGE;
    private int playButtonId = NO_IMAGE;
    private int audioId;

    public Word(int imageId, String en, String fr, int audioId, int playButtonId) {
        this.imageId = imageId;
        this.englishWord = en;
        this.frenchWord = fr;
        this.audioId = audioId;
        this.playButtonId = playButtonId;
    }

    public Word(int imageId, String en, String fr, int audioId) {
        this.imageId = imageId;
        this.englishWord = en;
        this.frenchWord = fr;
        this.audioId = audioId;
    }

    public Word(int imageId, String en, String fr) {
        this.imageId = imageId;
        this.englishWord = en;
        this.frenchWord = fr;
    }

    public Word(String en, String fr) {
        this.englishWord = en;
        this.frenchWord = fr;
    }

    public void setEnglishWord(String englishWord) {
        this.englishWord = englishWord;
    }

    public String getEnglishWord() {
        return englishWord;
    }

    public void setFrenchWord(String frenchWord) {
        this.frenchWord = frenchWord;
    }

    public String getFrenchWord() {
        return frenchWord;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public void setPlayButtonId(int playButtonId) {
        this.playButtonId = playButtonId;
    }

    public int getPlayButtonId() {
        return playButtonId;
    }

    /**
     *  Checks is the current word has an image or not
     * @return true if it has an image to display, false otherwise
     */
    public boolean hasImage(){
        return imageId != NO_IMAGE;
    }

    public int getAudioId() {
        return audioId;
    }

    public void setAudioId(int audioId) {
        this.audioId = audioId;
    }
}
