package com.thekarinsproject.translator;


import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ColorsFragment extends Fragment {

    MediaPlayer mMediaPlayer;

    // initialization of audio attributes and focus request
    AudioManager mAudioManager;

    final Object focusLock = new Object(); // No idea what is this

    boolean playbackDelayed = false;
    boolean playbackNowAuthorized = false;
    boolean resumeOnFocusGain = false;


    /**
     * Creates a new listener that manages the focus of the MediaPlayer, pausing, resuming
     * or stopping the player depending on it, this happens after the access
     * to the Audio system is granted */
    AudioManager.OnAudioFocusChangeListener mOnAudioFocusChangeListener =
            new AudioManager.OnAudioFocusChangeListener() {
                @Override
                public void onAudioFocusChange(int focusChange) {

                    switch(focusChange) {
                        case AudioManager.AUDIOFOCUS_GAIN:
                            if(playbackDelayed || resumeOnFocusGain){
                                synchronized (focusLock) {
                                    playbackDelayed = false;
                                    resumeOnFocusGain = false;
                                }
                                mMediaPlayer.start();
                            }
                            break;
                        case AudioManager.AUDIOFOCUS_LOSS:
                            synchronized (focusLock) {
                                resumeOnFocusGain = false;
                                playbackDelayed = false;
                            }
                            releaseMediaPlayer();
                            break;
                        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                            synchronized (focusLock) {
                                resumeOnFocusGain = true;
                                playbackDelayed = false;
                            }
                            mMediaPlayer.pause();
                            mMediaPlayer.seekTo(0);
                            break;
                    }
                }
            };

    AudioAttributes mAudioAttributes;
    AudioFocusRequest mAudioFocusRequest;
    /**
     * This listener gets triggered when the {@link MediaPlayer}
     * has completed playing the audio file */

    private MediaPlayer.OnCompletionListener mCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            releaseMediaPlayer();
        }
    };

    public ColorsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_colors,container, false);

        final ArrayList<Word> colors = new ArrayList<>();

        colors.add(new Word(R.drawable.color_red,"Red", "Rouge", R.raw.rouge));
        colors.add(new Word(R.drawable.color_green,"Green", "Vert (m) - Verte (f)", R.raw.vert));
        colors.add(new Word(R.drawable.color_blue,"Blue", "Bleu (m) - Bleue (f)", R.raw.bleu));
        colors.add(new Word(R.drawable.color_black,"Black", "Noir (m) - Noire(f)", R.raw.noir));
        colors.add(new Word(R.drawable.color_white,"White", "Blanc (m) - Blanche (f)", R.raw.blanc));
        colors.add(new Word(R.drawable.color_orange,"Orange", "Orange", R.raw.orange));
        colors.add(new Word(R.drawable.color_dusty_yellow,"Brown", "Marron", R.raw.marron));
        colors.add(new Word(R.drawable.color_purple,"Purple", "Violette", R.raw.violet));
        colors.add(new Word(R.drawable.color_cyan,"Cyan", "Bleu ciel", R.raw.bleuciel));
        colors.add(new Word(R.drawable.color_magenta,"Pink", "Rose", R.raw.rose));

        WordAdapter itemsAdapter = new WordAdapter(getActivity(), colors, R.color.category_colors);

        ListView listView = rootView.findViewById(R.id.list);

        listView.setAdapter(itemsAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // releases the media player if it currently exists because we will play another sound file
                releaseMediaPlayer();

                // Initialising audio elements;
                mAudioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
                mAudioAttributes = new AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_MEDIA)
                        .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                        .build();
                mAudioFocusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN_TRANSIENT)
                        .setAudioAttributes(mAudioAttributes)
                        .setAcceptsDelayedFocusGain(true)
                        .setOnAudioFocusChangeListener(mOnAudioFocusChangeListener)
                        .build();

                /* Verifies if audio focus has been granted or not */
                int response = mAudioManager.requestAudioFocus(mAudioFocusRequest);
                synchronized(focusLock) {
                    if(response == AudioManager.AUDIOFOCUS_REQUEST_FAILED) {
                        playbackNowAuthorized = false;
                    } else if (response == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                        playbackNowAuthorized = true;
                        mMediaPlayer = MediaPlayer.create(getActivity(), colors.get(position).getAudioId());
                        mMediaPlayer.start();
                    } else if (response == AudioManager.AUDIOFOCUS_REQUEST_DELAYED) {
                        playbackDelayed = true;
                        playbackNowAuthorized = false;
                    }
                }
                //Toast.makeText(NumberActivity.this, "List clicked", Toast.LENGTH_SHORT).show(); // Displays a message
                mMediaPlayer.setOnCompletionListener(mCompletionListener);
            }
        });

        return rootView;
    }

    // One of lifecycle states: stops the player when the app is not being interacted
    @Override
    public void onStop() {
        super.onStop();
        releaseMediaPlayer();
    }

    public void releaseMediaPlayer() {
        if(mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
            // Releases the focus
            mAudioManager.abandonAudioFocusRequest(mAudioFocusRequest);
        }
    }

}
