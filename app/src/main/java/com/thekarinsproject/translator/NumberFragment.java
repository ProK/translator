package com.thekarinsproject.translator;


import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class NumberFragment extends Fragment {

    MediaPlayer mMediaPlayer;

    // initialization of audio attributes and focus request
    AudioManager mAudioManager;

    final Object focusLock = new Object(); // No idea what is this

    boolean playbackDelayed = false;
    boolean playbackNowAuthorized = false;
    boolean resumeOnFocusGain = false;

    AudioAttributes mAudioAttributes;
    AudioFocusRequest mAudioFocusRequest;

    AudioManager.OnAudioFocusChangeListener mOnAudioFocusChangeListener =
            new AudioManager.OnAudioFocusChangeListener() {
                @Override
                public void onAudioFocusChange(int focusChange) {

                    switch(focusChange) {
                        case AudioManager.AUDIOFOCUS_GAIN:
                            if(playbackDelayed || resumeOnFocusGain){
                                synchronized (focusLock) {
                                    playbackDelayed = false;
                                    resumeOnFocusGain = false;
                                }
                                mMediaPlayer.start();
                            }
                            break;
                        case AudioManager.AUDIOFOCUS_LOSS:
                            synchronized (focusLock) {
                                resumeOnFocusGain = false;
                                playbackDelayed = false;
                            }
                            releaseMediaPlayer();
                            break;
                        case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                            synchronized (focusLock) {
                                resumeOnFocusGain = true;
                                playbackDelayed = false;
                            }
                            mMediaPlayer.pause();
                            mMediaPlayer.seekTo(0);
                            break;
                    }
                }
            };

    public NumberFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // We inflate the xml layout to the fragment
        View rootView = inflater.inflate(R.layout.activity_number, container, false);

        final ArrayList<Word> words = new ArrayList<>();

        words.add(new Word(R.drawable.number_one,"One", "Un", R.raw.un_une));
        words.add(new Word(R.drawable.number_two,"Two", "Deux", R.raw.deux));
        words.add(new Word(R.drawable.number_three,"Three", "Trois", R.raw.trois));
        words.add(new Word(R.drawable.number_four,"Four", "Quatre", R.raw.quatre));
        words.add(new Word(R.drawable.number_five,"Five", "Cinq", R.raw.cinq));
        words.add(new Word(R.drawable.number_six,"Six", "Six", R.raw.six));
        words.add(new Word(R.drawable.number_seven,"Seven", "Sept", R.raw.sept));
        words.add(new Word(R.drawable.number_eight,"Eight", "Huit", R.raw.huit));
        words.add(new Word(R.drawable.number_nine,"Nine", "Neuf", R.raw.neuf));
        words.add(new Word(R.drawable.number_ten,"Ten", "Dix", R.raw.dix));

        final WordAdapter itemsAdapter = new WordAdapter(getActivity(), words, R.color.category_numbers);

        final ListView listView = rootView.findViewById(R.id.list);

        listView.setAdapter(itemsAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // releases the media player if it currently exists because we will play another sound file
                releaseMediaPlayer();

                // Initialising audio elements;
                mAudioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
                mAudioAttributes = new AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_MEDIA)
                        .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                        .build();
                mAudioFocusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN_TRANSIENT)
                        .setAudioAttributes(mAudioAttributes)
                        .setAcceptsDelayedFocusGain(true)
                        .setOnAudioFocusChangeListener(mOnAudioFocusChangeListener)
                        .build();

                /* Verifies if audio focus has been granted or not */
                int response = mAudioManager.requestAudioFocus(mAudioFocusRequest);
                synchronized(focusLock) {
                    if(response == AudioManager.AUDIOFOCUS_REQUEST_FAILED) {
                        playbackNowAuthorized = false;
                    } else if (response == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                        playbackNowAuthorized = true;
                        mMediaPlayer = MediaPlayer.create(getActivity(), words.get(position).getAudioId());
                        mMediaPlayer.start();
                    } else if (response == AudioManager.AUDIOFOCUS_REQUEST_DELAYED) {
                        playbackDelayed = true;
                        playbackNowAuthorized = false;
                    }
                }
                //Toast.makeText(NumberActivity.this, "List clicked", Toast.LENGTH_SHORT).show(); // Displays a message
                mMediaPlayer.setOnCompletionListener(mCompletionListener);
            }
        });

        // Then we return the view inflated
        return rootView;
    }

    @Override
    public void onStop() {
        super.onStop();
        releaseMediaPlayer();
    }

    private MediaPlayer.OnCompletionListener mCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            releaseMediaPlayer();
        }
    };

    public void releaseMediaPlayer() {
        if(mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
            // Releases the focus
            mAudioManager.abandonAudioFocusRequest(mAudioFocusRequest);
        }
    }
}
