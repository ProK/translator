package com.thekarinsproject.translator;

import android.app.Activity;
import android.media.MediaPlayer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;

public class WordAdapter extends ArrayAdapter<Word> {

    private int mColorResourceId;

    /**
     *
     * @param context       The current context or android activity
     * @param wordsList     The list of words to load into the layout
     *
     */

    public WordAdapter(Activity context, ArrayList<Word> wordsList) {
        /* Initialize the ArrayAdapter's internal storage
        * The second arg (resource) is only used when the Adapter is populating a single TextView */
        super(context, 0, wordsList);
    }

    /**
     *
     * @param context       The current context or android activity
     * @param wordsList     The list of words to load into the layout
     * @param color       The color the view will have
     *
     */
    public WordAdapter (Activity context, ArrayList<Word> wordsList, int color) {
        super(context, 0, wordsList);
        mColorResourceId = color;
    }

    /**
     *
     * @param position      The position in the list of data.
     * @param convertView   The recycled view to populate.
     * @param parent        The parent ViewGroup that is used inflation
     * @return              The View for the position in the AdapterView.
     */

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        // Check if the existing view is being reused, otherwise inflate (fill) the view
        View listItemView = convertView;
        if(listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item_layout, parent, false);

        }

        // Gets the current word from the list's position
        Word currentWord = getItem(position);

        // Gets the TextView where the word is going to appear
        TextView englishWordTextView = listItemView.findViewById(R.id.englishWord);

        // Prints the english word in the TextView
        englishWordTextView.setText(currentWord.getEnglishWord());

        TextView frenchWordTextView = listItemView.findViewById(R.id.françaisWord);

        frenchWordTextView.setText(currentWord.getFrenchWord());

        ImageView imageView = listItemView.findViewById(R.id.iconImage);

        if(currentWord.hasImage()) {
            imageView.setImageResource(currentWord.getImageId());

            imageView.setVisibility(View.VISIBLE);
        }
        else
            imageView.setVisibility(View.GONE);

        // Sets the background color of the view
        View textContainer = listItemView.findViewById(R.id.text_container);

        // Gets the icon in layout
        ImageView playView = listItemView.findViewById(R.id.playButton);

        // Finds the color that the resource ID maps to
        int color = ContextCompat.getColor(getContext(), mColorResourceId);

        playView.setBackgroundColor(color);

        //Set the background color of the text container View
        textContainer.setBackgroundColor(color);

        // returns the whole layout.
        return listItemView;
    }
}
